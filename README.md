# Jetconf - Resolver

[JetConf](https://github.com/CZ-NIC/jetconf) Backend for [Knot Resolver](https://www.knot-resolver.cz/) based on [resolvers-yang](https://gitlab.labs.nic.cz/labs/resolvers-yang) resolvers management data model and library.

## Installation

```bash
$ git clone https://gitlab.labs.nic.cz/jetconf/jetconf-resolver
$ cd jetconf-resolver
$ python3 setup.py install
```

### To run as non-root
Configure unix socket group.
Add `SocketGroup=knot-resolver` to kresd socket config file uder `[Socket]` section
```bash
$ systemd edit --full kresd-control@.socket
```
Add user to `knot-resolver` group
```bash
$ usermod -a -G knot-resolver username
```

## Running with JetConf

Install [Knot Resolver](https://www.knot-resolver.cz/download/)

Run Kresd using systemd
```bash
$ systemctl start kresd@jetconf-resolver.service
```

You can try connect to Kresd Unix socket via terminal
```bash
socat - UNIX-CONNECT:/run/knot-resolver/control@jetconf-resolver
```

Example configuration file named `config-example.yaml` is located in `examples/jetconf-conf` directory.

```bash
$ cd examples/jetconf-conf
$ jetconf -c config-example.yaml
```

### Curl
Get data:
```bash
$ curl --http2 -k --cert-type PEM -E example-client.pem -X GET https://localhost:8444/restconf/data
```
Another Curl examples are located in `tests` directory.

## Links
* [Wiki](https://gitlab.labs.nic.cz/jetconf/jetconf-resolver/wikis/home)
* [JetConf](https://github.com/CZ-NIC/jetconf)
* [Knot Resolver](https://www.knot-resolver.cz/)
* [resolvers-yang](https://gitlab.labs.nic.cz/labs/resolvers-yang)

