import os
import codecs
from setuptools import setup, find_packages

here = os.path.abspath(os.path.dirname(__file__))
with codecs.open(os.path.join(here, 'README.md'), encoding='utf-8') as readme:
    long_description = readme.read()

setup(
    name='jetconf_resolver',
    packages=find_packages(),
    include_package_data=True,
    use_scm_version=True,
    setup_requires=["setuptools_scm"],
    description='JetConf backend for DNS resolvers - Knot Resolver.',
    long_description=long_description,
    url="https://gitlab.labs.nic.cz/jetconf/jetconf-resolver",
    author="Ales Mrazek",
    author_email="ales.mrazek@nic.cz",
    install_requires=["jetconf"],
    keywords=["RESTCONF", "yang", "data model", "JetConf"],
    classifiers=[
        "Programming Language :: Python :: 3.5",
        "Development Status :: 3 - Alpha",
        "Intended Audience :: System Administrators",
        "Intended Audience :: Telecommunications Industry",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: System :: Monitoring",
        "Topic :: System :: Systems Administration"
    ],
    package_data={
        "": ["yang-library-data.json"]
    }

)




