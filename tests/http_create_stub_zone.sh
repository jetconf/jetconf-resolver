#!/bin/bash

# Adds new "stub-zone"

CLIENT_CERT="../examples/jetconf-conf/example-client_curl.pem"

echo "--- POST new 'stub-zone'"
POST_DATA="@payload/new_stub_zone.json"
URL="https://localhost:8444/restconf/data/cznic-resolver-common:dns-resolver/resolver/stub-zones"

curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST -d "$POST_DATA" "$URL"

echo "--- conf-commit"
URL="https://localhost:8444/restconf/operations/jetconf:conf-commit"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST "$URL"



