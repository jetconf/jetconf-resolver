from jetconf_resolver.kresd_socket_api import KresdSocket
import pprint

# start kresd
KresdSocket.start_kresd()

k_socket = KresdSocket()

# OPERATIONS
print("OPERATIONS")
resolve = k_socket.resolve(".", "DNSKEY", "IN")
if resolve == "true":
    print("Resolve op:  OK")
else:
    print("Resolve op:  FAIL")

# SERVER
print("\nSERVER")
user = k_socket.user(username="knot-resolver")
if user == "true":
    print("User:        OK")
else:
    print("User:        FAIL")

# NETWORK
print("\nNETWORK")

interfaces = [{"name": "lo", "ip-address": "127.0.0.1"},
              {"name": "lo", "ip-address": "::1"},
              {"name": "eno1", "ip-address": "195.113.220.109", "port": 53}]

interfaces_cmd = k_socket.net_interfaces(interfaces)
if interfaces:
    print("Interface:   OK")
else:
    print("Interface:   FAIL")

out_v4 = k_socket.net_outgoing_v4("127.0.0.1")
if k_socket.net_outgoing_v4() == "127.0.0.1":
    print("Outgoing v4: OK")
else:
    print("Outgoing v4: FAIL")

out_v6 = k_socket.net_outgoing_v6("2001:db8:0:2::1")
if k_socket.net_outgoing_v6() == "2001:db8:0:2::1":
    print("Outgoing v6: OK")
else:
    print("Outgoing v6: FAIL")

do_ipv6 = k_socket.net_do_ipv6(True)
if bool(k_socket.net_do_ipv6()) is True:
    print("Do ipv6:     OK")
else:
    print("Do ipv6:     FAIL")

do_ipv4 = k_socket.net_do_ipv4(True)
if bool(k_socket.net_do_ipv4()) is True:
    print("Do ipv4:     OK")
else:
    print("Do ipv4:     FAIL")

buff_size = k_socket.net_bufsize(4096)
if int(k_socket.net_bufsize()) == 4096:
    print("Buffer size: OK")
else:
    print("Buffer size: FAIL")

# RESOLVER
print("\nRESOLVER")
#stub = k_socket.stub_zones()
print("Stub:        FAIL")

hint = k_socket.hint("localhost 127.0.0.1")
if hint == "[result] => true":
    print("Hint:        OK")
else:
    print("Hint:        FAIL")

hosts = k_socket.hints_hosts_file("/etc/hosts")
if hosts == "[result] => true":
    print("Host file:   OK")
else:
    print("Host file:   FAIL")

root_file = k_socket.root_zone_file("/etc/knot-resolver/root.hints")
if k_socket.root_zone_file() == "":
    print("Root file:   OK")
else:
    print("Root file:   FAIL")


r_hints = [{"name": "a.root-servers.net", "values": ["198.41.0.4", "2001:503:ba3e::2:30"]},
           {"name": "b.root-servers.net", "values": ["198.41.0.5", "2001:503:ba3e::2:20"]}]


root_hint = k_socket.root_hints(r_hints)
if k_socket.root_hints() == r_hints:
    print("Root hint:   OK")
else:
    print("Root hint:   FAIL")

glue = k_socket.option_glue_mode("strict")
if k_socket.option_glue_mode() == "strict":
    print("Glue mode:   OK")
else:
    print("Glue mode:   FAIL")

allow_local = k_socket.option_allow_local(True)
if bool(k_socket.option_allow_local()) is True:
    print("Allow local: OK")
else:
    print("Allow local: FAIL")

no_minimize = k_socket.option_no_minimize(True)
if bool(k_socket.option_no_minimize()) is True:
    print("No minimize: OK")
else:
    print("No minimize: FAIL")

reorder_rr = k_socket.reorder_rr(False)
if k_socket.reorder_rr() == "false":
    print("Reorder RR:  OK")
else:
    print("Reorder RR:  FAIL")

# LOGGING
print("\nLOGGING")

k_socket.verbosity(True)
if bool(k_socket.verbosity()) is True:
    print("Verbosity:   OK")
else:
    print("Verbosity:   FAIL")

# DNSSEC
print("\nDNSSEC")

add_ta = k_socket.add_trust_anchor_rr(". IN DS 19036 8 2 49AAC11D7B6F6446702E54A160"
                                      "7371607A1A41855200FD2CE1CDDE32F24E8FB5")
if bool(add_ta) is True:
    print("Add TA:      OK")
else:
    print("Add TA:      FAIL")

keysets = k_socket.trust_anchors_keysets()
print("TA keysets:  ")
pprint.pprint(keysets)

ta_insecure = k_socket.trust_anchors_insecure(["bad.example.com", "worse.example.com"])
if k_socket.trust_anchors_insecure() == ["bad.example.com", "worse.example.com"]:
    print("TA insecure: OK")
else:
    print("TA insecure: FAIL")

# CACHE
print("\nCACHE")

max_ttl = k_socket.cache_max_ttl(150)
if int(k_socket.cache_max_ttl()) == 150:
    print("Max ttl:     OK")
else:
    print("Max ttl:     FAIL")

min_ttl = k_socket.cache_min_ttl(5)
if int(k_socket.cache_min_ttl()) == 5:
    print("Min ttl:     OK")
else:
    print("Min ttl:     FAIL")

set_cache_size = k_socket.cache_size(1048576000)
if int(k_socket.cache_size()) == 1048576000:
    print("Size:        OK")
else:
    print("Size:        FAIL")


prefill_cmd = [{"origin": ".",
                "url": "https://www.internic.net/domain/root.zone",
                "ca-file": "/etc/pki/tls/certs/ca-bundle.crt"}]

prefill = k_socket.cache_prefill(prefill_cmd)
if k_socket.cache_prefill() == "true":
    print("Prefill:     OK")
else:
    print("Prefill:     FAIL")

# DNS64
print("\nDNS64")


dns64 = k_socket.dns64_config("64:ff9b::")
if dns64 == "\n> ":
    print("Prefix:      OK")
else:
    print("Prefix:      FAIL")

# stop kresd
KresdSocket.stop_kresd()
