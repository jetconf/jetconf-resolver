#!/bin/bash

# This will delete 'listen-interface' "new" from configuration

CLIENT_CERT="../examples/jetconf-conf/example-client_curl.pem"

echo "--- DEL 'listen-interface'"
URL="https://localhost:8444/restconf/data/cznic-resolver-common:dns-resolver/network/listen-interfaces=lo4"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X DELETE "$URL"

echo "--- conf-commit"
URL="https://localhost:8444/restconf/operations/jetconf:conf-commit"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST "$URL"



