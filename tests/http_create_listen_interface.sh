#!/bin/bash

# Adds new "listen-interface"

CLIENT_CERT="../examples/jetconf-conf/example-client_curl.pem"

echo "--- POST new 'listen-interfaces'"
POST_DATA="@payload/new_listen_interface.json"
URL="https://localhost:8444/restconf/data/cznic-resolver-common:dns-resolver/network/listen-interfaces"

curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST -d "$POST_DATA" "$URL"

echo "--- conf-commit"
URL="https://localhost:8444/restconf/operations/jetconf:conf-commit"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST "$URL"



