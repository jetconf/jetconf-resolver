#!/bin/bash

# This will delete 'stub-zone' "newstub.example.com" from configuration

CLIENT_CERT="../examples/jetconf-conf/lojza_curl.pem"

echo "--- DEL 'stub-zone'"
URL="https://127.0.0.1:8444/restconf/data/cznic-resolver-common:dns-resolver/resolver/stub-zones=newstub.example.com"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X DELETE "$URL"

echo "--- conf-commit"
URL="https://127.0.0.1:8444/restconf/operations/jetconf:conf-commit"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST "$URL"
