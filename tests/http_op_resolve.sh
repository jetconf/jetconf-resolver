#!/bin/bash

# rpc operation resolve query

CLIENT_CERT="../examples/jetconf-conf/example-client_curl.pem"
POST_DATA="@payload/resolve_input.json"
URL="https://localhost:8444/restconf/operations/cznic-resolver-common:resolve"

echo "--- Operation 'resolve'"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST -d "$POST_DATA" "$URL"
