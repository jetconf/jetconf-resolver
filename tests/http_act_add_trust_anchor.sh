#!/bin/bash

# action add-trust-anchor

CLIENT_CERT="../examples/jetconf-conf/example-client_curl.pem"
POST_DATA="@payload/add_trust_anchor_input_ds.json"
#POST_DATA="@payload/add_trust_anchor_input_dnskey.json"


URL="https://localhost:8444/restconf/data/cznic-resolver-common:dns-resolver/dnssec/trust-anchors=./add-trust-anchor"
#URL2="https://localhost:8444/restconf/data/cznic-resolver-common:dns-resolver/cache"

echo "--- Action 'add-trust-anchor'"
curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X POST -d "$POST_DATA" "$URL"

#curl --http2 -k --cert-type PEM -E ${CLIENT_CERT} -X GET "$URL2"
