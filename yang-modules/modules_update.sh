#!/usr/bin/env bash

declare -a modules_files=(
"cznic-dns-parameters"
"cznic-dns-rdata"
"cznic-resolver-common"
"cznic-resolver-knot"
"cznic-resolver-unbound"
)

modules_url="https://gitlab.labs.nic.cz/labs/resolvers-yang/raw/master/yang-modules/"

if [[ "$1" = "" ]]; then
    path=""
else
	path=$1"/"

fi

for i in "${modules_files[@]}"
    do
        # check for old version and rename it
        if [[ $(find ${path} -name ${i}".yang") ]]; then
            mv ${path}${i}".yang" ${path}${i}"-old.yang"
        fi
        # download file to path directory or current directory
        if [[ "$path" = "" ]]; then
            wget ${modules_url}${i}".yang"
        else
            wget -P ${path} ${modules_url}${i}".yang"
        fi
        # check if file was created and delete old version
        if [[ $(find ${path} -name ${i}".yang") ]] && [[ $(find ${path} -name ${i}"-old.yang") ]]; then
            rm ${path}${i}"-old.yang"
        elif [[ ! $(find ${path} -name ${i}".yang") ]]; then
            echo "Failed to download file: "${i}
            exit 1
        fi
    done

echo "--------------- DONE ---------------"
