from colorlog import info, error
from jetconf import config

from . import shared_objs as so


def jc_startup():
    info("JetConf-Resolver Backend: init")


def jc_end():
    info("JetConf-Resolver Backend:: cleaning up")

    if so.KRESD.config["AUTOSTART"]:
        # stop kresd
        so.KRESD.stop_service()
        info("Knot-Resolver stopped, service: {}".format(so.KRESD.config['SERVICE_NAME']))

    so.KRESD = None





