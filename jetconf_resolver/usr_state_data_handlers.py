# handlers for state data

from colorlog import info, error, debug

from yangson.instance import InstanceRoute

from jetconf.helpers import ErrorHelpers, LogHelpers, JsonNodeT, PathFormat
from jetconf.handler_base import StateDataHandlerBase, StateDataContainerHandler, StateDataListHandler
from jetconf.data import BaseDatastore
from resolvers_yang.parser import TrustAnchorRR

from . import shared_objs as so

# debug modul
debug_stateh = LogHelpers.create_module_dbg_logger(__name__)

# ---------- User-defined handlers follow ----------


# handler for current cache size
class KeyFileStateHandler(StateDataContainerHandler):
    def generate_node(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        debug_stateh(self.__class__.__name__ + ", ii = {}".format(node_ii))

        domain = node_ii[3].keys.get(("domain", None))

        key_file = so.KRESD.get_ta_keyfile(domain)

        return key_file


# handler for current cache size
class CacheSizeStateHandler(StateDataContainerHandler):
    def generate_node(self, node_ii: InstanceRoute, username: str, staging: bool) -> JsonNodeT:
        debug_stateh(self.__class__.__name__ + ", ii = {}".format(node_ii))

        cache_size = so.KRESD.get_cache_size()

        return cache_size


# Instantiate state data handlers
def register_state_handlers(ds: BaseDatastore):
    cs = CacheSizeStateHandler(ds, "/cznic-resolver-common:dns-resolver/cache/current-size")
    ta = KeyFileStateHandler(ds, "/cznic-resolver-common:dns-resolver/dnssec/trust-anchors/key-file")

    ds.handlers.state.register(cs)
    ds.handlers.state.register(ta)




