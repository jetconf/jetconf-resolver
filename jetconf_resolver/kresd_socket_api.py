import socket
import os
import copy
from re import compile
from colorlog import info, error
from jetconf.errors import BackendError
from jetconf.helpers import ErrorHelpers, LogHelpers, JsonNodeT

from resolvers_yang.parser import TrustAnchorRR
from resolvers_yang.kresd import kresd_conf_gen

from .models import domain_trust_anchors

# debug modul
debug_kresd = LogHelpers.create_module_dbg_logger(__name__)

def_conf = {
    "AUTOSTART": False,
    "LOAD_KRESD_CONF": False,
    "SOCKET_PATH": "/run/knot-resolver",
    "SERVICE_NAME": "jetconf-resolver"
}


class KnotResolver:

    def __init__(self):
        self.config = None                      # type: dict
        self.version = None                     # type: str
        self.unix_socket = None                 # type: KresdSocket

    def run(self, configuration=None):
        self.config = def_conf

        if configuration is not None:
            self._load_config(configuration)

        if self.config['AUTOSTART']:
            self.start_service()

        socket_path = self.config['SOCKET_PATH']+"/control@"+self.config['SERVICE_NAME']
        self.unix_socket = KresdSocket(socket_path)

    def stop(self):
        if self.config['AUTOSTART']:
            self.stop_service()

    def _load_config(self, conf: dict):
        for conf_key in conf.keys():
            print(conf_key)
            try:
                self.config[conf_key] = conf[conf_key]
            except (KeyError, TypeError) as ke:
                error("Knot-Resolver: Cannot set configuration, reason: {}".format(ke))

    def get_version(self):
        try:
            self.version = self.unix_socket.send_cmd("package_version()")
            return self.version
        except SocketConnectionError as nc:
            error("Knot-Resolver: Cannot get Knot-Resolver version, reason: {}".format(nc))
            return "No version find out"

    ############## START/STOP KNOT-RESOLVER ##############
    def start_service(self):
        try:
            cmd = "sudo systemctl start kresd@{0}".format(self.config['SERVICE_NAME'])
            os.system(cmd)
            info("Knot-Resolver: Started, service: {}".format(self.config['SERVICE_NAME']))
        except OSError as e:
            error("Knot-Resolver: Failed to Start Knot Resolver, {0}".format(e))

    def stop_service(self):
        try:
            cmd = "sudo systemctl stop kresd@{0}".format(self.config['SERVICE_NAME'])
            os.system(cmd)
            info("Knot-Resolver: Stopped, service: {}".format(self.config['SERVICE_NAME']))
        except OSError as e:
            error("Knot-Resolver: Failed to Stop Knot Resolver, {0}".format(e))

    ############# LOAD/SET FULL CONFIGURATION #############
    def set_resolver_conf(self, conf: JsonNodeT):
        kresd_conf = kresd_conf_gen.generate(conf)
        kresd_conf_list = kresd_conf.splitlines()
        print(kresd_conf)
        info("Knot-Resolver: setting up configuration to Knot-Resolver:")
        for line in kresd_conf_list:
            self.unix_socket.send_cmd(line)

    def get_resolver_conf(self) -> JsonNodeT:
        pass

    ################# OPERATIONS/ACTIONS #################
    def resolve_op(self, name: str, rr_type: str, dns_class: str = "IN"):
        dns_class = "kres.class." + dns_class
        rr_type = "kres.type." + rr_type
        cmd = "resolve('{0}', {1}, {2})".format(name, rr_type, dns_class)

        return self.unix_socket.send_cmd(cmd)

    def add_trust_anchor_act(self, rr: str):
        cmd = "trust_anchors.add('{0}')".format(rr)

        return self.unix_socket.send_cmd(cmd)

    ##################### STATE DATA #####################
    def get_trust_anchor_keysets(self):

        owner_regex = compile(r'^\[(?P<ta_domain>.*)\] => {$')
        domains = []
        for line in self.unix_socket.send_cmd("trust_anchors.keysets").splitlines():
            match = owner_regex.search(line)
            if match:
                domains.append(str(match.group(1)))

        ta_keysets = []
        for domain in domains:
            trust_anchor = self.get_trust_anchor(domain)

            ta_keysets.append(copy.deepcopy(trust_anchor))

        return ta_keysets

    def get_trust_anchor(self, domain: str):

        if domain == ".":
            domain = "\\0"
        else:
            domain = self.unix_socket.str2dname(domain)

        def ta_cmd(owner: str, index: int = None, prop: str = None):
            if index is None:
                cmd = "trust_anchors.keysets['{0}'].{1}".format(owner, prop)
            elif prop is None:
                cmd = "trust_anchors.keysets['{0}'][{1}]".format(owner, index)
            else:
                cmd = "trust_anchors.keysets['{0}'][{1}].{2}".format(owner, index, prop)
            return cmd

        ta_keyset = []
        i = 1
        while self.unix_socket.send_cmd(ta_cmd(domain, i)) != "nil":
            rr = ta_cmd(domain, i)
            rr_string = self.unix_socket.rr2str(rr)
            tr_anchor = {"id": int(i - 1)}

            if 'DNSKEY' in rr_string:
                dnskey = TrustAnchorRR.parse_dnskey(rr_string)
                tr_anchor['dnskey'] = copy.deepcopy(dnskey['dnskey'])

            elif 'DS' in rr_string:
                ds = TrustAnchorRR.parse_ds(rr_string)
                tr_anchor['ds'] = copy.deepcopy(ds['ds'])
            else:
                break

            ta_keyset.append(copy.deepcopy(tr_anchor))
            i += 1

        auto_update = True

        domain_ta = domain_trust_anchors.copy()
        domain_ta['domain'] = self.unix_socket.dname2str(domain)
        domain_ta['auto-update'] = auto_update
        domain_ta['trust-anchor'] = ta_keyset

        key_file = self.unix_socket.send_cmd(ta_cmd(owner=domain, prop="filename"))
        if key_file == 'nil':
            del domain_ta['key-file']
        else:
            domain_ta['key-file'] = key_file

        return domain_ta

    def get_ta_keyfile(self, domain: str):

        if domain == ".":
            dname = "\\0"
        else:
            dname = self.unix_socket.str2dname(domain)

        cmd = "trust_anchors.keysets['{0}'].filename".format(dname)
        key_file = self.unix_socket.send_cmd(cmd)

        return key_file

    def get_cache_size(self):
        cmd = "cache.current_size"
        return self.unix_socket.send_cmd(cmd)

    ##################### CONF DATA #####################



##################### ERRORS #####################
class KresdSocketError(BackendError):
    pass


class SocketConnectionError(KresdSocketError):
    pass


class KresdSocket:

    def __init__(self, socket_path):
        self.kr_socket = socket.socket()
        self.path = socket_path  # type: str
        self.kr_socket.settimeout(5)

    def set_timeout(self, timeout: float):
        self.kr_socket.settimeout(timeout)

    def send_cmd(self, cmd):
        try:
            self.kr_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            self.kr_socket.connect(self.path)
        except (OSError, PermissionError, socket.error) as e:
            error("Knot-Resolver: socket connection failed {0}, {1}".format(self.path, e))

        cmd_encode = (cmd + "").encode('UTF-8')
        data = b'> '

        try:
            self.kr_socket.sendall(cmd_encode)
        except (socket.error, OSError) as se:
            error("Knot-Resolver: failed to send socket command '{0}', reason: {1}".format(cmd, se))

        try:
            while data == b'> ':
                data = self.kr_socket.recv(4096)

        except socket.timeout as se:
            error("Knot-Resolver: no response to socket command '{0}' received, reason: {1}".format(cmd, se))

        try:
            self.kr_socket.close()
        except OSError as ose:
            error("Knot-Resolver: cannot close socket, reason: {}".format(ose))

        response = KresdSocket.remove_sufix(data.decode('UTF-8'))
        debug_kresd("KresdSocket.send_cmd\n"
                    "command:\n{0}\n"
                    "response:\n{1}".format(cmd, response))

        return response

    ############# AUXILIARY FUNCTIONS ##############
    @staticmethod
    def remove_sufix(string_with_suf: str):

        # check and remove prefixes
        while string_with_suf.startswith('\n') or string_with_suf.startswith('> '):
            while string_with_suf.startswith('\n'):
                string_with_suf = string_with_suf[1:]

            if string_with_suf.startswith('> '):
                string_with_suf = string_with_suf[2:]

        # check and remove sufixes
        while string_with_suf.endswith('> ') or string_with_suf.endswith('\n'):
            if string_with_suf.endswith('> '):
                string_with_suf = string_with_suf[:-2]

            while string_with_suf.endswith('\n'):
                string_with_suf = string_with_suf[:-1]

        return string_with_suf

    def dname2str(self, dname: str) -> str:
        cmd = "kres.dname2str('{0}')".format(dname)
        result = self.send_cmd(cmd)
        return result

    def str2dname(self, dname: str) -> str:
        cmd = "kres.str2dname({0})".format(dname)
        result = self.send_cmd(cmd)
        return result

    def rr2str(self, rr: str) -> str:
        cmd = "kres.rr2str({0})".format(rr)
        result = self.send_cmd(cmd)
        return result


