from colorlog import info, debug

from yangson.instance import InstanceRoute
from jetconf.helpers import JsonNodeT, LogHelpers
from jetconf.data import BaseDatastore

from resolvers_yang.parser import TrustAnchorRR

from . import shared_objs as so

# debug modul
debug_acth = LogHelpers.create_module_dbg_logger(__name__)

# ---------- User-defined actions handlers follow ----------


class ActionHandlersContainer:
    def __init__(self, ds: BaseDatastore):
        self.ds = ds

    def add_trust_anchor_act(self, ii: InstanceRoute, input_args: JsonNodeT, username: str) -> JsonNodeT:

        rr_string = ""

        root = self.ds.get_data_root()
        ta_node = root.goto(ii[0:-1])

        domain = ta_node['domain'].value
        debug_acth("Called action 'add-trust-anchor' for domain '{0}' by user '{1}':".format(domain, username))

        if 'ds' in input_args:
            rr_string = TrustAnchorRR.create_ds_string(domain, input_args['ds'])
        elif 'dnskey' in input_args:
            rr_string = TrustAnchorRR.create_dnskey_string(domain, input_args['dnskey'])

        reply = so.KRESD.add_trust_anchor_act(rr_string)

        return reply


def register_action_handlers(ds: BaseDatastore):
    act_handlers_obj = ActionHandlersContainer(ds)
    ds.handlers.action.register(act_handlers_obj.add_trust_anchor_act,
                                "/cznic-resolver-common:dns-resolver/dnssec/trust-anchors/add-trust-anchor")
