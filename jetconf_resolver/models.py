domain_trust_anchors = {
    "domain": str,
    "auto-update": bool,
    "key-file": str,
    "trust-anchor": []
}

ta_ds = {
    "id": int,
    "ds": {
        "algorithm": str,
        "digest": str,
        "digest-type": str,
        "key-tag": int
    }
}

ta_dnskey = {
    "id": int,
    "dnskey": {
        "algorithm": str,
        "flags": str,
        "protocol": int,
        "public-key": str
    }
}

re_hints = {
    "name": str,
    "values": [str]
}

