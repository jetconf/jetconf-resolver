import json
import os
from colorlog import error, info, debug
from yangson.instance import NonexistentInstance
from jetconf import config
from jetconf.helpers import ErrorHelpers
from jetconf.data import JsonDatastore

from .kresd_socket_api import KnotResolver, KresdSocket, KresdSocketError
from . import shared_objs as so


class UserDatastore(JsonDatastore):

    def load(self):
        super().load()

        # Create global API objects
        so.KRESD = KnotResolver()

        if 'KRESD' in config.CFG.root:
            so.KRESD.run(config.CFG.root["KRESD"])
        else:
            so.KRESD.run()

        info("Knot-Resolver version: {}".format(so.KRESD.get_version()))

        # Read Knot-Resolver configuration and save it to the datastore
        if so.KRESD.config["LOAD_KRESD_CONF"]:
            # try to load configuration from Knot-Resolver to Datastore
            try:
                kresd_readed_conf = so.KRESD.get_resolver_conf()
                new_root = self._data.put_member("cznic-resolver-common:dns-resolver",
                                                 kresd_readed_conf["cznic-resolver-common:dns-resolver"],
                                                 raw=True).top()
                self.set_data_root(new_root)
                info("Kresd-Socket: Knot-Resolver configuration has been loaded to Datastore")
            except KresdSocketError as e:
                error("Kresd-Socket: Cannot load Knot-Resolver configuration, reason: {0}".format(e))
        else:
            # try to set datastore configuration to Knot Resolver
            try:
                new_ta_list = []
                ta_list = self._data['cznic-resolver-common:dns-resolver']['dnssec']['trust-anchors']
                for ta in ta_list:
                    if bool(ta['auto-update'].value) is True:
                        new_ta = so.KRESD.get_trust_anchor(ta['domain'].value)
                        new_ta_list.append(new_ta.copy())
                    else:
                        new_ta_list.append(ta.value)
                print(new_ta_list)
                # generating mix of state and configuration trust-anchors
                new_root = self._data['cznic-resolver-common:dns-resolver']['dnssec']\
                    .put_member("trust-anchors", new_ta_list, raw=True).top()
                self.set_data_root(new_root)

                root_data = self.get_data_root()
                root_data.validate()
                so.KRESD.set_resolver_conf(root_data.add_defaults())
                info("Knot-Resolver: Datastore configuration data have been set to Knot-Resolver")
            except KresdSocketError as e:
                error("Knot-Resolver: Cannot set configuration data to Knot-Resolver, reason: {0}".format(e))

    def save(self):
        if so.KRESD.config["LOAD_KRESD_CONF"]:
            info("Saving NACM data to JSON {}".format(self.json_file))

            # Just need to save NACM data,
            # everything else is loaded dynamically on startup or when requested
            try:
                nacm_raw = self._data["ietf-netconf-acm:nacm"].raw_value()
                data_raw = {"ietf-netconf-acm:nacm": nacm_raw}
            except NonexistentInstance as nee:
                # NACM data not present
                data_raw = {}
                debug("Cannot save NACM, data not present {}".format(nee))

            with open(self.json_file, "w") as jfd:
                json.dump(data_raw, jfd, indent=2)

        else:
            info("Saving data to JSON {}".format(self.json_file))

            # else save all data
            super().save()

