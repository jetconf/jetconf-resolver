from colorlog import info, debug

from yangson.instance import InstanceRoute
from yangson.exceptions import NonexistentInstance

from jetconf.helpers import JsonNodeT, PathFormat, LogHelpers
from jetconf.data import BaseDatastore
from jetconf_resolver.kresd_socket_api import KresdSocket
from . import shared_objs as so

# debug modul
debug_oph = LogHelpers.create_module_dbg_logger(__name__)

# ---------- User-defined handlers follow ----------


class OpHandlersContainer:
    def __init__(self, ds: BaseDatastore):
        self.ds = ds

    @staticmethod
    def resolve_op(input_args: JsonNodeT, username: str):

        debug_oph("Called operation 'resolve' by user '{}':".format(username))

        domain_name = input_args.get('cznic-resolver-common:name')
        rr_type = input_args.get('cznic-resolver-common:type')
        dns_class = input_args.get('cznic-resolver-common:class')

        reply = so.KRESD.resolve_op(domain_name, rr_type, dns_class)

        return reply


def register_op_handlers(ds: BaseDatastore):
    op_handlers_obj = OpHandlersContainer(ds)
    ds.handlers.op.register(op_handlers_obj.resolve_op, "cznic-resolver-common:resolve")




