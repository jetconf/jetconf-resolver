# handlers for configuration data
import json
from colorlog import info, error
from typing import List, Dict, Union, Any

from yangson.instance import InstanceRoute

from jetconf.data import BaseDatastore, DataChange
from jetconf.helpers import ErrorHelpers, LogHelpers
from jetconf.handler_base import ConfDataListHandler, ConfDataObjectHandler

from resolvers_yang.kresd import kresd_conf_gen

from . import shared_objs as so
from.kresd_socket_api import KresdSocketError

JsonNodeT = Union[Dict[str, Any], List]
epretty = ErrorHelpers.epretty
# debug modul
debug_confh = LogHelpers.create_module_dbg_logger(__name__)


# ---------- User-defined handlers follow ----------


class RootObjectHandler(ConfDataObjectHandler):

    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")
        base_nv = self.ds.get_data_root()

        print(ch.input_data)

        try:
            root_data = self.ds.get_data_root()
            root_data.validate()
            so.KRESD.set_resolver_conf(root_data.add_defaults())
            info("Knot-Resolver: Datastore configuration data have been set to Knot-Resolver")

        except KresdSocketError as e:
            error("Knot-Resolver: Cannot set configuration data to Knot-Resolver, reason: {0}".format(e))

        # try:
        #     new_ta_list = []
        #     ta_list = base_nv['cznic-resolver-common:dns-resolver']['dnssec']['trust-anchors'].value
        #     for ta in ta_list:
        #         if bool(ta['auto-update']) is True:
        #             new_ta = so.KRESD.get_trust_anchor(ta['domain'])
        #             new_ta_list.append(new_ta.copy())
        #         else:
        #             new_ta_list.append(ta)
        #     # generating mix of state and configuration trust-anchors
        #     #new_root = base_nv['cznic-resolver-common:dns-resolver']['dnssec']["trust-anchors"] = new_ta_list
        #     # TODO: not working
        #     print(new_ta_list)
        #     new_root = base_nv['cznic-resolver-common:dns-resolver']['dnssec'] \
        #         .put_member("trust-anchors", new_ta_list, raw=True).top()
        #     self.ds.set_data_root(new_root)
        #
        #     root_data = self.ds.get_data_root()
        #     root_data.validate()
        #     so.KRESD.set_resolver_conf(root_data.add_defaults())
        #     info("Knot-Resolver: Datastore configuration data have been set to Knot-Resolver")
        # except KresdSocketError as e:
        #     error("Knot-Resolver: Cannot set configuration data to Knot-Resolver, reason: {0}".format(e))

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")


class ServerObjectHandler(ConfDataObjectHandler):

    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")


class NetworkObjectHandler(ConfDataObjectHandler):

    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")


class ResolverObjectHandler(ConfDataObjectHandler):

    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")


class LoggingObjectHandler(ConfDataObjectHandler):

    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")


class DnssecObjectHandler(ConfDataObjectHandler):

    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")


class CacheObjectHandler(ConfDataObjectHandler):

    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")


class DebuggingObjectHandler(ConfDataObjectHandler):

    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")


class Dns64ObjectHandler(ConfDataObjectHandler):

    def create(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " create triggered")

    def replace(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " replace triggered")

    def delete(self, ii: InstanceRoute, ch: DataChange):
        debug_confh(self.__class__.__name__ + " delete triggered")


def commit_begin():

    debug_confh("Starting new Knot Resolver configuration generation")


def commit_end(failed: bool = False):

    if failed:
        debug_confh("Aborting Knot Resolver configuration generation")
    else:
        debug_confh("Committing Knot Resolver configuration generation")


def register_conf_handlers(ds: BaseDatastore):
    ds.handlers.conf.register(RootObjectHandler(ds, "/cznic-resolver-common:dns-resolver"))

    #ds.handlers.conf.register(ServerObjectHandler(ds, "/cznic-resolver-common:dns-resolver/server"))
    #ds.handlers.conf.register(NetworkObjectHandler(ds, "/cznic-resolver-common:dns-resolver/network"))
    #ds.handlers.conf.register(ResolverObjectHandler(ds, "/cznic-resolver-common:dns-resolver/resolver"))
    #ds.handlers.conf.register(LoggingObjectHandler(ds, "/cznic-resolver-common:dns-resolver/logging"))
    #ds.handlers.conf.register(DnssecObjectHandler(ds, "/cznic-resolver-common:dns-resolver/dnssec"))
    #ds.handlers.conf.register(CacheObjectHandler(ds, "/cznic-resolver-common:dns-resolver/cache"))
    #ds.handlers.conf.register(DebuggingObjectHandler(ds, "/cznic-resolver-common:dns-resolver/debugging"))
    #ds.handlers.conf.register(Dns64ObjectHandler(ds, "/cznic-resolver-common:dns-resolver/dns64"))

    # commit callbacks
    ds.handlers.commit_begin = commit_begin
    ds.handlers.commit_end = commit_end

