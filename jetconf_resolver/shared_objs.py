import logging
from .kresd_socket_api import KnotResolver
from jetconf.helpers import ErrorHelpers, LogHelpers

KRESD = None    # type: KnotResolver
