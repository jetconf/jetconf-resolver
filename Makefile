PROJECT = jetconf_resolver
VERSION = 0.1
.PHONY = tags modules-update deps install-deps test

tags:
	find $(PROJECT) -name "*.py" | etags -

modules-update:
	chmod u+x yang-modules/modules_update.sh
	./yang-modules/modules_update.sh yang-modules

deps:
	mv requirements.txt requirements.txt.old
	pip freeze > requirements.txt

install-deps:
	pip install -r requirements.txt

test:
	@py.test tests
